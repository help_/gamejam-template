extends "res://scripts/ProximityEventArea.gd"

func _input(event):
	if  isPlayerInRange and event.is_action_pressed("interact"):
		print_debug("INTERACTED")
		var id = get_parent().get_meta("id")
		DoorsData.load_scenes(id)
		var broken = DoorsData.is_locked(id)
		
		if broken:
			var error_code = get_tree().change_scene_to_file("res://scenes/wirebox.tscn")
			if error_code != OK:
				print("ERROR: ", error_code)
		else:
			print_debug("wires fixed")
			DoorsData.set_next_door()
			var scene = DoorsData.get_next_scene()
			var error_code = get_tree().change_scene_to_file(scene)
			if error_code != OK:
				print("ERROR: ", error_code)
