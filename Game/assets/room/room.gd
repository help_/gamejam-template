extends RigidBody2D

var size

func make_room(_pos, _size):
	position = _pos
	size = _size
	var s = RectangleShape2D.new()
	s.extents = size*1.2
	$CollisionShape2D.shape = s

func freeze():
	freeze_mode = RigidBody2D.FREEZE_MODE_STATIC
