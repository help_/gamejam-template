extends Node2D

var Room =  preload("res://assets/room/room.tscn")

var tile_size = 32
var num_rooms = 20
var min_size = 4
var max_size = 10
var cull = 0.5

var path_var: AStar2D #astar

var Map: TileMap

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	path_var = await make_rooms()
	Map = $roomTiles
	make_map()


func make_rooms():
	for i in range(num_rooms):
		var pos = Vector2(0,0)
		var r = Room.instantiate()
		var w = min_size + randi() % (max_size - min_size)
		var h = min_size + randi() % (max_size - min_size)
		r.make_room(pos, Vector2(w, h)*tile_size)
		$rooms.add_child(r)
		
	var room_pos = []
	await get_tree().create_timer(1).timeout
	for room in $rooms.get_children():
		if randf()< cull:
			room.queue_free()
		else:
			room.freeze()
			room_pos.append(Vector2(room.position.x, room.position.y))
	await get_tree().create_timer(1).timeout
	var _path = find_mst(room_pos)
	return _path
	
func find_mst(nodes):
	var path = AStar2D.new()
	path.add_point(path.get_available_point_id(), nodes.pop_front())
	while nodes:
		var min_dist = INF
		var min_p = null
		var p = null
		
		for p1 in path.get_point_ids():
			var p1_pos = path.get_point_position(p1)
			
			for p2 in nodes:
				if p1_pos.distance_to(p2)< min_dist:
					min_dist = p1_pos.distance_to(p2)
					min_p = p2
					p = p1_pos
		var n = path.get_available_point_id()
		path.add_point(n, min_p)
		path.connect_points(path.get_closest_point(p), n)
		nodes.erase(min_p)
	return path

func _draw():
	for room in $rooms.get_children():
		draw_rect(Rect2(room.position - room.size, room.size * 2), Color(2, 228,  0 ), false)
	if path_var:
		for p in path_var.get_point_ids():
			for c in path_var.get_point_connections(p):
				var pp = path_var.get_point_position(p)
				var cp = path_var.get_point_position(c)
				draw_line(pp, cp, Color(1,1,0),15,true)
				
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	queue_redraw( )
	
func _input(event):
	if event.is_action_pressed('interact'):
		for n in $rooms.get_children():
			n.queue_free()
		make_rooms()
		path_var = await make_rooms()
		Map = $roomTiles
		make_map()
		
func make_map():
	# Creates a TileMap from the generated rooms & path
	# find_start_room()
	# find_end_room()
	Map.clear()

	# Fill TileMap with walls and carve out empty spaces
#   
# 	var full_rect = Rect2()
#    for room in $Rooms.get_children():
#        var r = Rect2(room.position-room.size,
#                    room.get_node("CollisionShape2D").shape.extents*2)
#        full_rect = full_rect.merge(r)
#    var topleft = Map.world_to_map(full_rect.position)
#    var bottomright = Map.world_to_map(full_rect.end)
#    for x in range(topleft.x, bottomright.x):
#        for y in range(topleft.y, bottomright.y):
#            Map.set_cell(x, y, 1)
	# Carve rooms and corridors
	var corridors = []  # One corridor per connection
	for room in $rooms.get_children():
		var s = (room.size / tile_size).floor()
		var pos = Map.local_to_map(room.position)
		var ul = (room.position/tile_size).floor() - s
		for x in range(2, s.x * 2-1):
			for y in range(2, s.y * 2-1):
				Map.set_cell(0,Vector2i(ul.x+x, ul.y+y), 1, Vector2i(0,0), 0 )

		# Carve corridors
		var p = path_var.get_closest_point(room.position, false)
		for conn in path_var.get_point_connections(p):
			if not conn in corridors:
				var start = Map.local_to_map(Vector2(path_var.get_point_position(p).x, path_var.get_point_position(p).y))
				var end = Map.local_to_map(Vector2(path_var.get_point_position(conn).x, path_var.get_point_position(conn).y))
				carve_path(start, end)
		corridors.append(p)

func carve_path(pos1, pos2):
	# Carves a path between two points
	var x_diff = sign(pos2.x - pos1.x)
	var y_diff = sign(pos2.y - pos1.y)
	if x_diff == 0: x_diff = pow(-1.0, randi() % 2)
	if y_diff == 0: y_diff = pow(-1.0, randi() % 2)
	# Carve either x/y or y/x
	var x_y = pos1
	var y_x = pos2
	if (randi() % 2) > 0:
		x_y = pos2
		y_x = pos1
	for x in range(pos1.x, pos2.x, x_diff):
		Map.set_cell(0,Vector2i(x, x_y.y), 1, Vector2i(0,0), 0 )
		Map.set_cell(0,Vector2i(x, x_y.y+y_diff), 1, Vector2i(0,0), 0 )  # widen the corridor
	for y in range(pos1.y, pos2.y, y_diff):
		Map.set_cell(0, Vector2i(y_x.x, y), 1, Vector2i(0,0), 0 )
		Map.set_cell(0, Vector2i(y_x.x+x_diff, y), 1, Vector2i(0,0), 0 )  # widen the corridor
