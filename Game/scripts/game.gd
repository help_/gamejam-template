extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	$ElectricalBox.play()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var is_broken = DoorsData.is_locked($ElectricalBox.get_meta("id"))
	if is_broken:
		$GeneralDarkness.show()
	else:
		$GeneralDarkness.hide()
