extends Sprite2D

var target: Vector2
var smoothed_pos: Vector2
var hidden_angle = 145
var win_threshold = 10
var shake2_threshold = 30
var shake1_threshold = 60

# Shake parameters
var shakeDuration = 1  # Duration of the shake in seconds
var shakeMagnitude = 2  # Magnitude of the shake (maximum displacement in pixels)
var shakeSpeed = 10     # Speed of the shake animation (number of times it oscillates per second)

## Shake animation variables
var shakeTimer = 0.0
var initialPosition = Vector2.ZERO

var numberGuesses = 5


# Called when the node enters the scene tree for the first time.
func _ready():
	hidden_angle = 1.0 #randf_range(0.0, 360.0)
	initialPosition = position


func _process(delta):
	smoothed_pos = lerp(smoothed_pos, get_global_mouse_position(), 0.5 )
	look_at(smoothed_pos)
	
	if shakeTimer > 0:
		shakeTimer -= delta  # Decrease the shake timer

		# Calculate the displacement based on time and parameters
		var progress = 1.0 - shakeTimer / shakeDuration
		var angle = progress * TAU * shakeSpeed
		var displacement = Vector2(cos(angle), sin(angle)) * shakeMagnitude

		# Apply the displacement to the node's position
		position = initialPosition + displacement

		if shakeTimer <= 0:
			position = initialPosition  # Reset the position after the shake
	else:
		smoothed_pos = lerp(smoothed_pos, target, 0.01 )
		look_at(smoothed_pos)

func start_shake_1():
	shakeDuration = 1
	shakeTimer = shakeDuration  # Start the shake timer

func start_shake_2():
	shakeDuration = 2
	shakeTimer = shakeDuration  # Start the shake timer
	
func _input(event):
	if Input.is_action_just_released("interact"):
		var angle = fmod(rotation*180/PI, 360.0)
		if angle < 0:
			angle = fmod(abs(angle), 360.0)
			angle = 360-angle
		print("guess:", angle)
		var dif = angle - hidden_angle
		if dif > 180:
			dif -= 360
		else: if dif < -180:
			dif += 360
		dif = abs(dif)
		print("dif:",dif)
		if win_threshold > dif:
			print("win")
			DoorsData.set_unlocked()
			DoorsData.set_next_door()
			var scene = DoorsData.get_next_scene()
			get_tree().change_scene_to_file(scene)
		else: if shake2_threshold > dif:
			start_shake_1()
		else: if shake1_threshold > dif:
			start_shake_2()
		
		numberGuesses -= 1
		if numberGuesses<=0:
			print("lose")
			var scene = DoorsData.get_previous_scene()
			get_tree().change_scene_to_file("res://scenes/game.tscn")
