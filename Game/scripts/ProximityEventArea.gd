extends Area2D

var isPlayerInRange

# Called when the node enters the scene tree for the first time.
func _ready():
	$ButtonBubble.visible = false
	isPlayerInRange = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_body_entered(body):
	$ButtonBubble.visible = true
	isPlayerInRange = true


func _on_body_exited(body):
	$ButtonBubble.visible = false
	isPlayerInRange = false


func _input(event):
	if  isPlayerInRange and event.is_action_pressed("interact"):
		print_debug("INTERACTED")
		
