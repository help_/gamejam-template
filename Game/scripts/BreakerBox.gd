extends AnimatedSprite2D

var first_click = null
var second_click = null
var first_click_array_index = null

var wire_spots = Array()
var wires_to_draw = Array()

# Called when the node enters the scene tree for the first time.
func _ready():
	#load wire_spots into the array left to right, top down
	wire_spots.append(Vector2(-18, -17))
	wire_spots.append(Vector2(18, -20))
	wire_spots.append(Vector2(-17, -1))
	wire_spots.append(Vector2(16, -3))
	wire_spots.append(Vector2(-17, 9))
	wire_spots.append(Vector2(14, 9))
	wire_spots.append(Vector2(-18, 18))
	wire_spots.append(Vector2(17, 22))

	wires_to_draw.append(false)
	wires_to_draw.append(false)
	wires_to_draw.append(false)
	wires_to_draw.append(false)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for i in range(0, wires_to_draw.size()):
		if !wires_to_draw[i]:
			return
	DoorsData.set_unlocked()
	DoorsData.set_next_door()
	var scene = DoorsData.get_next_scene()
	get_tree().change_scene_to_file(scene)
	
func _draw():
	for i in range(0, wires_to_draw.size()):
		#check if wire is getting drawn here
		if wires_to_draw[i]:
			draw_line(wire_spots[i * 2], wire_spots[i * 2 + 1], Color.html("263238"), 2)
	
	
	#draw_line(wire_spots[0], wire_spots[1], Color.html("263238"), 1) #top line
	#draw_line(wire_spots[2], wire_spots[3], Color.html("263238"), 1) #second line
	#draw_line(wire_spots[4], wire_spots[5], Color.html("263238"), 1) #third line
	#draw_line(wire_spots[6], wire_spots[7], Color.html("263238"), 1) #fourth line
	
func _input(event):
	#mouse_pos = get_global_mouse_position()
	if !event.is_action_pressed("click"):
		return
		
	if first_click == null:
		first_click = get_global_mouse_position()
	elif second_click == null:
		second_click = get_global_mouse_position()
		
	if first_click == null:
		return
		
	#check if valid clicks
	if first_click_array_index == null:
		for i in range(0, wire_spots.size()):
			#var position_global_calc = Vector2((wire_spots[i].x * scale) + position.x, (wire_spots[i].y * scale) + position.y)
			if first_click.distance_to(position_global_calc(wire_spots[i])) < 20:
				first_click_array_index = i
				break
		if first_click_array_index == null:
			first_click = null	
		return	
	
	if second_click == null:
		return
		
	if first_click_array_index % 2 == 1:
		#odd index, match is one less
		if(second_click.distance_to(position_global_calc(wire_spots[first_click_array_index - 1]))) > 20:
			second_click = null
			return
	else:
		#even index, match is one more
		if(second_click.distance_to(position_global_calc(wire_spots[first_click_array_index + 1]))) > 20:
			second_click = null
			return
		
	if second_click != null:
		var success_row = first_click_array_index / 2
		wires_to_draw[success_row] = true
		reset_clicks()
			
func position_global_calc(position_before_calc):
	var my_calse = scale
	var my_pos = position
	var x_value = (position_before_calc.x * scale.x) + position.x
	var y_value = (position_before_calc.y * scale.y) + position.y
	var after_calc = Vector2(x_value, y_value)
	
	return after_calc
	
func reset_clicks():
	first_click = null
	second_click = null
	first_click_array_index = null
