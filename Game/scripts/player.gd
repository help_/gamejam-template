extends CharacterBody2D

@export var speed = 40 #player speed in pixels/sec
const JUMP_VELOCITY = -400.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

var target

func _ready():
	var door_id = DoorsData.get_current_door()
	var doors = get_owner().get_children()
	for door in doors:
		if door.get_meta("id") == door_id:
			position = door.position
			
	target = position
	$AnimatedSprite2D.animation = "char_2_out_of_breath"
	$AnimatedSprite2D.play()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	velocity = global_position.direction_to(target) * speed
	
	$PointLight2D.look_at(target)
	
	if global_position.distance_to(target) > 10:
		$AnimatedSprite2D.animation = "char_2_walk"
		move_and_slide()
	else:
		$AnimatedSprite2D.animation = "char_2_out_of_breath"
	if velocity.x != 0:
		$AnimatedSprite2D.flip_v = false
		$AnimatedSprite2D.flip_h = velocity.x < 0



func _input(event):
	if event.is_action_pressed("click"):
		target = get_global_mouse_position()

func setPosition(pos):
		position = pos
