extends Node
# gdscript doesnt support arrays of arrays
# 1, "res://scenes/game.tscn", 2, "res://scenes/room_electrical.tscn", false

var doors_data = [
	{id=0, loc="res://scenes/game.tscn",con_id=1, con_loc="res://scenes/room_electrical.tscn", locked=true}, 
	{id=1, loc="res://scenes/room_electrical.tscn",con_id=0, con_loc="res://scenes/game.tscn", locked=true},
	{id=2, loc="res://scenes/game.tscn", con_id=0, con_loc="res://scenes/game.tscn", locked=true},
]

var locked = false
var previous_Scene = "res://scenes/game.tscn"
var next_scene = "res://scenes/room_electrical.tscn"

var previous_door_id = 0
var next_door_id = 0
var current_door_id = 0


func is_locked(_id):
	var door_val = doors_data[_id]
	return door_val.locked

func set_unlocked():
	doors_data[previous_door_id].locked = false
	doors_data[next_door_id].locked = false
	
func get_previous_scene():
	return previous_Scene

func get_next_scene():
	return next_scene
	
func get_current_door():
	return current_door_id

func load_scenes(_id):
	print(_id)
	locked = doors_data[_id].locked
	previous_Scene = doors_data[_id].loc
	next_scene = doors_data[_id].con_loc
	previous_door_id = doors_data[_id].id
	next_door_id = doors_data[_id].con_id
	current_door_id = previous_door_id

func set_previous_door():
	current_door_id = previous_door_id
	
func set_next_door():
	current_door_id = next_door_id
	

