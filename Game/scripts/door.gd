extends Object

class_name Door
var id:int
var scene_location:String
var connected_id:int
var connected_scene_location:String
var locked: bool

func _init(_id,_loc, _c_id,_c_loc, _l):
	id = _id
	scene_location = _loc
	connected_id = _c_id
	connected_scene_location = _c_loc
	locked = _l
