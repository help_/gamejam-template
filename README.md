# GameJam Template

## Getting started

Welcome to the Broose's Server Game Jam! This covers many of the rule and expectations. Our goal is to have a fun building a SMALL game.  

## Legal

We are using the MIT License. All work that is apart of the Game Jam is opensource. As it is open source feel free to use any opensource resources. 

## Schedule
Game Jam Kick off  Friday, June 23 2023 @ 7:00pm 

Game Jam End  Saturday, July 1 2023 @ 11:59pm 

The game jam is spread accross the week to allow for those with work/family/life to participate. Collaberation can be in person or virtual.

## Game Idea Submission

The theme will be "Adventure!"

We will have a Kick off meeting Friday, June 23 2023 @ 7:00pm to pick a Game idea. Game ideas should be minimal in scope and catter to groups skillsets. Ideas ideally but are not limited to have a paragraph written covering basic Gameplay mechanics, artistic theme, or story. Ideas will be pitched and vetted for feasiblity. Ideas can be adjusted or refied with the group. Voting will be a simple majority. Tasks will developed and work will be on a volenteer basis. The general idea is everyone works on one game but time permitting other ideas can be worked on. 

## Game Jam Jobs list

Game jams require us to wear a lot of hats. Tasks are based on the needs of the originl Game Idea submission. Each of these task areas are its own disaplines and team members coulds spend the whole game jam focused on a specific area. As follows are not a complete list.

### Developer
This team member focuses on writing code. They mainly work in the game engine. They are in charge of building objects and new features to the game. Developer tasks deal with finding ways to implement game design features. 

### Game Designer
Game Designers take objects built by developers to build video game scenes. They mainly work in the engine using the built objects to design the scene. They decide what the level looks like, where elements should go. 

### Asset Artist
Asset Artist create assets to be used in the game. This could be 2D,3D, pixel art or sprites. The art style is decided by who is making the art. They also build out animations. They work in art tools like Asesrpite, blender or Photoshop. They work with developers to build Objects for Game Designers. 

### Music and Sound Engineer
Music and sound engineers find/compose music and audio for the game. They work in their perferd audio tools,like audacity or grageband. They work with Developers and Game Designers on where and how to best add sound to the game. Voice acting would fall into this catagory.  

### Narrative Officers
Narrative plays a huge role in story telling. Depending on the game and team talent a writer could be added. They work in wordpad or google docs. Narrative officers work together to build a story and work with Game Designers, Developers, Artists and Sound engineers on how to best tell that story. 

### Play testers 
Play testers are needed to test the game. The go through the game experience to make sure its fun! They work in browser or game engine. They work with everyone to make sure whats implemented is fun. 

## Tech Stack

The tech stack is a collection of tools and resources to help facilitate game building.

### Communication tool 
Discord - Broose's Server - Game Jam Stuff
general chat is for all coordination memes and voice is for meetings. Additonal spaces and voice rooms can be made as needed. 

### Game Engine - Godot
https://godotengine.org/download/windows/

Godot is an opensoure game engine. There are lots of resoreces to learn how to code for Godot, it uses a GDScript which is based on python. It has built in tools for multiplayer, physics, tilemapping, etc.

docs - https://docs.godotengine.org/en/stable/<br>

https://www.kodeco.com/37604834-godot-4-getting-started

### Configuration Management - Git/Gitlab
https://git-scm.com/downloads

git is a configiuration tool that will all us to share files and project data efficently. Gitlab is a remote repository that will host our code and allow our game to be hosted online.

good tutorial on using git with godot
https://www.youtube.com/playlist?list=PLCBLMvLIundB2axawTUWHySTeAD-bCfyg

### Game Dialogue engine - Ink 
https://www.inklestudios.com/ink/
https://godotengine.org/asset-library/asset/846

Ink is a dialogue system used for writing branching dialogue. 

### Asset Generation 
https://www.blender.org/ <br> 
https://www.aseprite.org/<br> 
https://minghai.github.io/MarioSequencer/<br> 
